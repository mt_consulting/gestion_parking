{
    "name": "Gestion Parking",
    "version": "1.0",
    "depends": ["base", 'account_voucher', 'account', 'account_followup', 'account_analytic_analysis'],
    "author": "RCA",
    "category": "Général",
    "description": """
    Ce module permet la gestion d'un parking
    
    La paroisse dispose d'un parking autour de la cathédrale.
Les personnes qui habitent autour de la cathédrale sont intéressées pour y garer leur (s) voiture (s).
D'autres personnes qui viennent faire des courses dans la ville se garent également sur ce parking qui appartient à la cathédrale.
Le curé veut installer une barrière à l'entrée du parking et faire payer une petite location pour l'entretien du parking.

Le curé veut depuis son pose avoir une vue de la situation financière du parking.
Le curé veut avoir une base de données contenant au moins les abonnés réguliers. Ce sont les personnes qui habitent autour et qui ont pris un abonnement mensuel pour le jour et / ou un abonnement mensuel pour la nuit.
Pour ceux là justement, on veut avoir une base de donnée pour gérer les situations suivantes :
Tous les 2 du mois 
    - Avoir une vue tree qui affiche les colonnes suivantes :
            Nom du client
            Type d'abonnement [Nuit, Jour] + Total de la colonne
            Montant de ce qu'il doit + Total de la colonne
            Montant de ce qu'il a payé + Total de la colonne
            Montant impayé + Total de la colonne
    - Si on clique sur le détail d'un client: 
            Voir sa fiche avec les mois payés sur une période de l 'année civile
            Les mois impayés
    - Une vue tree de ceux qui ont  payé (qui sont à jour) avec le total des versements
    - Une vue tree de ceux qui n'ont pas payé avec la liste des mensualtiés non acquitées.
    - Un imprimé de ces listing.
    """,
    "init_xml": [],

    'data': [
                "data/sql_currency.sql",
                "data/fleet_cars.xml",
                "data/acount_analytic_account.xml",
                "view/abonnes.xml",
                "view/vehicule.xml",
                "view/abonnement.xml",
                "view/invoice.xml",
                "view/voucher.xml",
                "report/layout.xml",
                "report/rapport_abonnement_impaye.xml",
                "report/rapport_abonnement_paye.xml",
                "wizard/reports.xml",
                "view/listing.xml",
                
                "view/menu.xml",
                   ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
