# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from openerp.tools.translate import _

class account_invoice(osv.osv):
    
    _inherit = "account.invoice"
    
    def _get_abn(self, cr, uid, ids, fields_name, args, context):
        res={} 
        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')
        for id in ids :
            invoice=invoice_obj.browse(cr, uid, id, context)
            invoice_line_ids=invoice_line_obj.search(cr, uid, [('invoice_id','=',invoice.id)])
            res[id]=invoice_line_obj.browse(cr, uid, invoice_line_ids, context).account_analytic_id.name
        return res  
     
    _columns = {
        'credit':  fields.float('Credit', digits=(12, 0)),
        'abn':fields.function(_get_abn, type='char',string='Abonnement', store=True),
        
        #'vehicule_id': fields.many2one('vehicule', 'Véhicule', help='Véhicule'),
        }
    
    def invoice_pay_customer(self, cr, uid, ids, context=None):
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'gestion_parking', 'view_vendor_receipt_dialog_form')

        inv = self.browse(cr, uid, ids[0], context=context)
        return {
            'name':_("Pay Invoice"),
            'view_mode': 'form',
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'account.voucher',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'payment_expected_currency': inv.currency_id.id,
                'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(inv.partner_id).id,
                'default_amount': inv.type in ('out_refund', 'in_refund') and -inv.residual or inv.residual,
                'default_reference': inv.name,
                'close_after_process': True,
                'invoice_type': inv.type,
                'invoice_id': inv.id,
                'default_journal_impot': inv.journal_id.id,
                'default_credit':inv.partner_id.payment_abonnement_due,
                'default_type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                'type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment'
            }
        }

   
account_invoice()
