# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from operator import itemgetter
import time
from openerp.osv.fields import many2one
from openerp.tools.translate import _
from openerp import api
import datetime
from dateutil.relativedelta import relativedelta
from openerp import netsvc
import openerp.addons.decimal_precision as dp

class partner(osv.osv):
    
    _name = 'res.partner'
    _inherit = 'res.partner'
    
    #   ============  abonnements  =====================
    def _get_amounts_abonnement_and_date(self, cr, uid, ids, name, arg, context=None):
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('name', '=', 'Abonnement')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = amount_due
                               
        return res

    def _get_payment_abonnement_amount_overdue(self, cr, uid, ids, name, arg, context=None):
       
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('name', '=', 'Abonnement')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = amount_overdue
                                
        return res

    def _get_payment_abonnement_earliest_due_date(self, cr, uid, ids, name, arg, context=None):
       
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('name', '=', 'Abonnement')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = worst_due_date
                                
        return res
    
    def _payment_abonnement_due_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        query, query_args = self._get_followup_overdue_query(cr, uid, args, overdue_only=False, context=context)
        cr.execute(query, query_args)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_abonnement_due > 0])]
        return result
     
    def _payment_abonnement_overdue_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        query, query_args = self._get_followup_overdue_query(cr, uid, args, overdue_only=True, context=context)
        cr.execute(query, query_args)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
#        return [('id','in', [x[0] for x in res])]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_abonnement_due > 0])]
        return result

    def _payment_abonnement_earliest_date_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        company_id = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.id
        having_where_clause = ' AND '.join(map(lambda x: '(MIN(l.date_maturity) %s %%s)' % (x[1]), args))
        having_values = [x[2] for x in args]
        query = self.pool.get('account.move.line')._query_get(cr, uid, context=context)
        cr.execute('SELECT partner_id FROM account_move_line l '\
                    'WHERE account_id IN '\
                        '(SELECT id FROM account_account '\
                        'WHERE type=\'receivable\' AND active) '\
                    'AND l.company_id = %s '
                    'AND reconcile_id IS NULL '\
                    'AND ' + query + ' '\
                    'AND partner_id IS NOT NULL '\
                    'GROUP BY partner_id HAVING ' + having_where_clause,
                     [company_id] + having_values)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
#        return [('id','in', [x[0] for x in res])]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_abonnement_due > 0])]
        return result

    def _hav_abonnement_fn(self, cursor, user, ids, name, arg, context=None):

        res = {}
        for partner in self.browse(cursor, user, ids, context=context):
            res[partner.id] = True
#             invoice_existence = False
            for journal in sale.impot_ids.periodicite.name:
                if journal != 'Abonnement':
                    res[partner.id] = False
            
        return res
        
    def _get_abonnement_list(self, cr, uid, ids, name, arg, context=None): 
        res = {}
        obj = self.pool.get('account.analytic.account')
        for id in ids:
            res[id] = obj.search(cr, uid, [('partner_id', '=', id), ('type', '=', 'contract'), ('recurring_rule_type', '=', 'monthly')])
        return res

    def _get_vehicule_list(self, cr, uid, ids, name, arg, context=None): 
        res = {}
        obj = self.pool.get('vehicule')
        for id in ids:
            res[id] = obj.search(cr, uid, [('driver_id', '=', id)])
        return res

    _description = 'Ce sont les personnes qui habitent autour de la  paroisse et qui ont pris un abonnement mensuel ou annuel pour le jour et / ou un abonnement mensuel ou annuel pour la nuit.'
    
    _defaults = {
             
               }
     
    _columns = {
          
#                 =============================   Abonnement   ===================================
        'payment_abonnement_due':fields.function(_get_amounts_abonnement_and_date, type='float', string="Montant d'Abonnement du",
                                                 fnct_search=_payment_abonnement_due_search, digits_compute=dp.get_precision('Account')),
        
        'payment_abonnement_amount_overdue':fields.function(_get_payment_abonnement_amount_overdue,
                                                 type='float', string="Montant d'Abonnement en retard",
                                                 fnct_search=_payment_abonnement_overdue_search, digits_compute=dp.get_precision('Account')),
                
        'payment_abonnement_earliest_due_date':fields.function(_get_payment_abonnement_earliest_due_date,
                                                    type='date',
                                                    string="Echeance d'Abonnement la plus en retard ",
                                                    fnct_search=_payment_abonnement_earliest_date_search, digits_compute=dp.get_precision('Account')),
        'abonnement_idss' : fields.function(_get_abonnement_list, type='one2many', obj='account.analytic.account', string="Abonnements"),
        'vehicule_idss' : fields.function(_get_vehicule_list, type='one2many', obj='vehicule', string="Véhicules"),
        }
        