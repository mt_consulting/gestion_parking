# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
from openerp import tools
from openerp.osv.orm import except_orm
from openerp.tools.translate import _

class vehicle_model_brand(osv.Model):
    _name = 'vehicule.model.brand'
    _description = 'Brand model of the vehicle'

    _order = 'name asc'

    def _get_image(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = tools.image_get_resized_images(obj.image)
        return result

    def _set_image(self, cr, uid, id, name, value, args, context=None):
        return self.write(cr, uid, [id], {'image': tools.image_resize_image_big(value)}, context=context)

    _columns = {
        'name': fields.char('Nom de la marque', required=True),
        'image': fields.binary("Logo",
            help="This field holds the image used as logo for the brand, limited to 1024x1024px."),
        'image_medium': fields.function(_get_image, fnct_inv=_set_image,
            string="Medium-sized photo", type="binary", multi="_get_image",
            store = {
                'vehicule.model.brand': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Medium-sized logo of the brand. It is automatically "\
                 "resized as a 128x128px image, with aspect ratio preserved. "\
                 "Use this field in form views or some kanban views."),
        'image_small': fields.function(_get_image, fnct_inv=_set_image,
            string="Smal-sized photo", type="binary", multi="_get_image",
            store = {
                'vehicule.model.brand': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Small-sized photo of the brand. It is automatically "\
                 "resized as a 64x64px image, with aspect ratio preserved. "\
                 "Use this field anywhere a small image is required."),
    }


class vehicle_model(osv.Model):

    def _model_name_get_fnc(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for record in self.browse(cr, uid, ids, context=context):
            name = record.modelname
            if record.brand_id.name:
                name = record.brand_id.name + ' / ' + name
            res[record.id] = name
        return res

    def on_change_brand(self, cr, uid, ids, model_id, context=None):
        if not model_id:
            return {'value': {'image_medium': False}}
        brand = self.pool.get('vehicule.model.brand').browse(cr, uid, model_id, context=context)
        return {
            'value': {
                'image_medium': brand.image,
            }
        }

    _name = 'vehicule.model'
    _description = 'Model of a vehicle'
    _order = 'name asc'

    _columns = {
        'name': fields.function(_model_name_get_fnc, type="char", string='Nom', store=True),
        'modelname': fields.char('Nom du Modèle', required=True), 
        'brand_id': fields.many2one('vehicule.model.brand', 'Modèle de voiture', required=True, help='Brand of the vehicle'),
        'image': fields.related('brand_id', 'image', type="binary", string="Logo"),
        'image_medium': fields.related('brand_id', 'image_medium', type="binary", string="Logo (medium)"),
        'image_small': fields.related('brand_id', 'image_small', type="binary", string="Logo (small)"),
    }
    
class vehicule(osv.osv):
    
    def _vehicle_name_get_fnc(self, cr, uid, ids, prop, unknow_none, context=None):
        res = {}
        for record in self.browse(cr, uid, ids, context=context):
            res[record.id] = record.model_id.brand_id.name + '/' + record.model_id.modelname + ' / ' + record.license_plate
        return res    

    def on_change_model(self, cr, uid, ids, model_id, context=None):
        if not model_id:
            return {}
        model = self.pool.get('vehicule.model').browse(cr, uid, model_id, context=context)
        return {
            'value': {
                'image_medium': model.image,
            }
        }
            
    _name = 'vehicule'
    _description = 'Information on a vehicle'
    _order= 'license_plate asc'
    _columns = {
        'name': fields.function(_vehicle_name_get_fnc, type="char", string='Name', store=True),
        'model_id': fields.many2one('vehicule.model', 'Modèle', required=True, help='Modèle de la véhicule'),
        'driver_id': fields.many2one('res.partner', 'Conducteur', help='Conducteur de la véhicule'),
        'license_plate': fields.char("Plaque d'Immatriculation", required=True, help='License plate number of the vehicle (ie: plate number for a car)'),
        'image': fields.related('model_id', 'image', type="binary", string="Logo"),
        'image_medium': fields.related('model_id', 'image_medium', type="binary", string="Logo (medium)"),
        'image_small': fields.related('model_id', 'image_small', type="binary", string="Logo (small)"),
        }
    