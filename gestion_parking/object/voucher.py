# -*- coding: utf-8 -*-
from openerp.osv import osv
from openerp.osv import fields, osv
from openerp import netsvc
from datetime import datetime
import datetime
from dateutil.relativedelta import relativedelta
import openerp.addons.decimal_precision as dp


class res_currency(osv.osv):
    _inherit =  "res.currency"
    _columns = {
                'description': fields.char('Description', size=64),
                }    
class account_voucher_line(osv.osv):    
 
    _inherit = 'account.voucher.line'
    
    _columns = {
                 'state':fields.selection(
                                          [('draft', 'Draft'),
                                           ('cancel', 'Cancelled'),
                                           ('proforma', 'Pro-forma'),
                                           ('posted', 'Posted')
                                           ], 'Status', readonly=True, size=32, track_visibility='onchange',
                                          help=' * The \'Draft\' status is used when a user is encoding a new and unconfirmed Voucher. \
                                                \n* The \'Pro-forma\' when voucher is in Pro-forma status,voucher does not have an voucher number. \
                                                \n* The \'Posted\' status is used when user create voucher,a voucher number is generated and voucher entries are created in account \
                                                \n* The \'Cancelled\' status is used when user cancel voucher.'),
         
                }
     
account_voucher_line()    

class account_voucher(osv.osv):    

    _inherit = 'account.voucher'

    _columns = {
                'journal_impot': fields.many2one('account.journal', 'Type impot'),
               # 'vehicule_id': fields.many2one('vehicule', 'Véhicule', help='Véhicule'),
                'credit':  fields.float('Montant en retard de paiement', digits_compute=dp.get_precision('Account')),
                'code_journal_impot':fields.char('journal_impot_code', size=64, required=False, readonly=False),
                }
    
    def recompute_voucher_lines(self, cr, uid, ids, partner_id, journal_id, price, currency_id, ttype, date, context=None):
        """
        Returns a dict that contains new values and context

        @param partner_id: latest value from user input for field partner_id
        @param args: other arguments
        @param context: context arguments, like lang, time zone

        @return: Returns a dict which contains new values, and context
        """
        def _remove_noise_in_o2m():
            """if the line is partially reconciled, then we must pay attention to display it only once and
                in the good o2m.
                This function returns True if the line is considered as noise and should not be displayed
            """
            if line.reconcile_partial_id:
                if currency_id == line.currency_id.id:
                    if line.amount_residual_currency <= 0:
                        return True
                else:
                    if line.amount_residual <= 0:
                        return True
            return False

        if context is None:
            context = {}
        context_multi_currency = context.copy()

        currency_pool = self.pool.get('res.currency')
        move_line_pool = self.pool.get('account.move.line')
        partner_pool = self.pool.get('res.partner')
        journal_pool = self.pool.get('account.journal')
        line_pool = self.pool.get('account.voucher.line')

        #set default values
        default = {
            'value': {'line_dr_ids': [], 'line_cr_ids': [], 'pre_line': False},
        }

        # drop existing lines
        line_ids = ids and line_pool.search(cr, uid, [('voucher_id', '=', ids[0])])
        for line in line_pool.browse(cr, uid, line_ids, context=context):
            if line.type == 'cr':
                default['value']['line_cr_ids'].append((2, line.id))
            else:
                default['value']['line_dr_ids'].append((2, line.id))

        if not partner_id or not journal_id:
            return default

        journal = journal_pool.browse(cr, uid, journal_id, context=context)
        partner = partner_pool.browse(cr, uid, partner_id, context=context)
        currency_id = currency_id or journal.company_id.currency_id.id
        
        journal_impot = context.get('default_journal_impot', journal_id)
        #name = journal_pool.browse(cr, uid, journal_impot, context=context).name
        
        
        total_credit = 0.0
        total_debit = 0.0
        account_type = None
        if context.get('account_id'):
            account_type = self.pool['account.account'].browse(cr, uid, context['account_id'], context=context).type
        if ttype == 'payment':
            if not account_type:
                account_type = 'payable'
            total_debit = price or 0.0
        else:
            total_credit = price or 0.0
            if not account_type:
                account_type = 'receivable'

        if not context.get('move_line_ids', False):
            ids = move_line_pool.search(cr, uid, [('state','=','valid'), ('account_id.type', '=', account_type), ('reconcile_id', '=', False), ('partner_id', '=', partner_id)], context=context)
        else:
            ids = context['move_line_ids']
        invoice_id = context.get('invoice_id', False)
        company_currency = journal.company_id.currency_id.id
        move_lines_found = []

        #order the lines by most old first
        ids.reverse()
        account_move_lines = move_line_pool.browse(cr, uid, ids, context=context)

        #compute the total debit/credit and look for a matching open amount or invoice
        for line in account_move_lines:
            if _remove_noise_in_o2m():
                continue

            if invoice_id:
                if line.invoice.id == invoice_id:
                    #if the invoice linked to the voucher line is equal to the invoice_id in context
                    #then we assign the amount on that line, whatever the other voucher lines
                    move_lines_found.append(line.id)
            elif currency_id == company_currency:
                #otherwise treatments is the same but with other field names
                if line.amount_residual == price:
                    #if the amount residual is equal the amount voucher, we assign it to that voucher
                    #line, whatever the other voucher lines
                    move_lines_found.append(line.id)
                    break
                #otherwise we will split the voucher amount on each line (by most old first)
                total_credit += line.credit or 0.0
                total_debit += line.debit or 0.0
            elif currency_id == line.currency_id.id:
                if line.amount_residual_currency == price:
                    move_lines_found.append(line.id)
                    break
                total_credit += line.credit and line.amount_currency or 0.0
                total_debit += line.debit and line.amount_currency or 0.0

        remaining_amount = price
        #voucher line creation
        for line in account_move_lines:

            if _remove_noise_in_o2m():
                continue

            if line.currency_id and currency_id == line.currency_id.id:
                amount_original = abs(line.amount_currency)
                amount_unreconciled = abs(line.amount_residual_currency)
            else:
                #always use the amount booked in the company currency as the basis of the conversion into the voucher currency
                amount_original = currency_pool.compute(cr, uid, company_currency, currency_id, line.credit or line.debit or 0.0, context=context_multi_currency)
                amount_unreconciled = currency_pool.compute(cr, uid, company_currency, currency_id, abs(line.amount_residual), context=context_multi_currency)
            line_currency_id = line.currency_id and line.currency_id.id or company_currency
            rs = {
                'name':line.move_id.name,
                'type': line.credit and 'dr' or 'cr',
                'move_line_id':line.id,
                'account_id':line.account_id.id,
                'amount_original': amount_original,
                'amount': (line.id in move_lines_found) and min(abs(remaining_amount), amount_unreconciled) or 0.0,
                'date_original':line.date,
                'date_due':line.date_maturity,
                'amount_unreconciled': amount_unreconciled,
                'currency_id': line_currency_id,
            }
            remaining_amount -= rs['amount']
            #in case a corresponding move_line hasn't been found, we now try to assign the voucher amount
            #on existing invoices: we split voucher amount by most old first, but only for lines in the same currency
            if not move_lines_found:
                if currency_id == line_currency_id:
                    if line.credit:
                        amount = min(amount_unreconciled, abs(total_debit))
                        rs['amount'] = amount
                        total_debit -= amount
                    else:
                        amount = min(amount_unreconciled, abs(total_credit))
                        rs['amount'] = amount
                        total_credit -= amount

            if rs['amount_unreconciled'] == rs['amount']:
                rs['reconcile'] = True

            if rs['type'] == 'cr':
                default['value']['line_cr_ids'].append(rs)
            else:
                default['value']['line_dr_ids'].append(rs)

            if len(default['value']['line_cr_ids']) > 0:
                default['value']['pre_line'] = 1
            elif len(default['value']['line_dr_ids']) > 0:
                default['value']['pre_line'] = 1
            default['value']['writeoff_amount'] = self._compute_writeoff_amount(cr, uid, default['value']['line_dr_ids'], default['value']['line_cr_ids'], price, ttype)
            
            code=journal_pool.browse(cr, uid, journal_impot, context=context).code
           # default['journal_impot']=journal_impot
            if(code == 'ABO'):
                 default['value']['credit'] = partner.payment_abonnement_due
        return default

    def write(self, cr, uid, ids, vals, context=None):
        if vals.get('state'):
            voucher_brw = self.browse(cr, uid, ids, context)[0]
            voucher_line_obj = self.pool.get('account.voucher.line')
            for line in voucher_brw.line_cr_ids:
                voucher_line_obj.write(cr, uid , line.id , {'state': vals.get('state')} , context=context)
        res = super(account_voucher, self).write(cr, uid, ids, vals, context=context)
        return res
     
     
     
    def action_move_line_create(self, cr, uid, ids, context=None):
        ret = super(account_voucher, self).action_move_line_create(cr, uid, ids, context=context)
        account_move_lines = self.pool.get('account.move.line')
        for voucher in self.browse(cr, uid, ids, context=context):
            for vl in voucher.line_ids:
                if vl.move_line_id:
                    for mvl in vl.move_line_id.move_id.line_id:
                        account_move_lines.write(cr, uid, [mvl.id], {'state':'valid'})
            for mvl in voucher.move_id.line_id:
                account_move_lines.write(cr, uid, [mvl.id], {'state':'valid'})
  
        return True

    def basic_onchange_partner(self, cr, uid, ids, partner_id, journal_id, ttype, context=None):
        partner_pool = self.pool.get('res.partner')
        journal_pool = self.pool.get('account.journal')
        res = {'value': {'account_id': False, 'credit': False}}
        if not partner_id or not journal_id:
            return res

        journal = journal_pool.browse(cr, uid, journal_id, context=context)
        partner = partner_pool.browse(cr, uid, partner_id, context=context)
        account_id = False
        if journal.type in ('sale','sale_refund'):
            account_id = partner.property_account_receivable.id
        elif journal.type in ('purchase', 'purchase_refund','expense'):
            account_id = partner.property_account_payable.id
        else:
            account_id = journal.default_credit_account_id.id or journal.default_debit_account_id.id

        res['value']['account_id'] = account_id
        res['value']['credit'] = partner.payment_abonnement_due
        return res
    
    def button_actualise(self, cr, uid, ids , context=None):
        acount_voucher_brw = self.browse(cr, uid, ids[0], context=context)
#                                   browse(cr, uid, journal_id, context=context)
        context.update({'search_default_customer': 1, 'type': 'receipt'})
        res = super(account_voucher, self).onchange_partner_id(cr, uid,
                                                               ids=ids,
                                                               partner_id=acount_voucher_brw.partner_id.id,
                                                               journal_id=acount_voucher_brw.journal_id.id,
                                                               amount=acount_voucher_brw.amount,
                                                               currency_id=acount_voucher_brw.currency_id.id,
                                                               ttype=acount_voucher_brw.type,
                                                               date=acount_voucher_brw.date,
                                                               context=context)
        
        
        account_voucher_line_obj = self.pool.get("account.voucher.line")
        for line in res['value']['line_cr_ids']:
            line.update({'voucher_id': ids[0]})
            account_voucher_line_obj.create(cr, uid, line, context=context)
        vals = {'credit':acount_voucher_brw.partner_id.payment_abonnement_due}
        self.write(cr, uid, ids, vals, context=context)
        return
     
account_voucher()   

class paiement_anticipe(osv.osv_memory):

    _name = "paiement.anticipe"
    
    def default_get(self, cr, uid, fields, context=None):
        res = super(paiement_anticipe, self).default_get(cr, uid, fields, context)
        actives = context.get('partner_id', []) 
        
        if actives:
            partner = self.pool.get('res.partner').browse(cr, uid, actives, context=context)
            res.update({'partner_id': partner.id})
          
        return res
    
    def _get_date_invoice(self, cr, user, ids, context={}):
        wiz = self.browse(cr, user, ids[0], context)
        date = datetime.now()
        newdate = date.replace(month=11)
        return newdate
        
    def paiement_anticipe(self, cr, user, ids, context={}):
        wiz = self.browse(cr, user, ids[0], context)
        partner = wiz.partner_id
        abonnement=wiz.abonnement_id
        period_debut=wiz.periode_debut
        periode_fin=wiz.periode_fin
        type = 'Abonnement'
#         date = datetime.now()
#         date = date.replace(day=1)
        ##########Ajouter par Rahma
        obj = self.pool.get('account.analytic.account')
        ids = obj.search(cr, user, [('name','=',abonnement.name),('state', '=', 'open'), ('amount_max', '!=', 0), ('partner_id', '=', partner.id)])
        invoice_line_obj = self.pool.get('account.invoice.line')
        invoice_line_ids=invoice_line_obj.search(cr, user, [('account_analytic_id','=',abonnement.id),('invoice_id.period_id','=',period_debut.id)])
        if(not invoice_line_ids):
                obj.recurring_create_invoice_pay_anticipe(cr, user, ids, period_debut , context)
        next_date = datetime.datetime.strptime(period_debut.date_start, "%Y-%m-%d")
        date_fin = datetime.datetime.strptime(periode_fin.date_start, "%Y-%m-%d")
        while (next_date!=date_fin):
            next_date = next_date+relativedelta(months=+1)
            obj_period = self.pool.get('account.period')
            ids_period = obj_period.search(cr, user, [('date_start','=',next_date)])
            period=obj_period.browse(cr, user, ids_period, context=context)
            invoice_line_ids=invoice_line_obj.search(cr, user, [('account_analytic_id','=',abonnement.id),('invoice_id.period_id','=',period.id)])
            if(not  invoice_line_ids):
                    obj.recurring_create_invoice_pay_anticipe(cr, user, ids, period , context)
       
        return {'type': 'ir.actions.act_window_close'}
    
    _columns = {
       
        'partner_id': fields.many2one('res.partner', 'Partner', readonly=True, required=True),
        'abonnement_id': fields.many2one('account.analytic.account','Abonnement', required=True),
        'date_invoice': fields.function(_get_date_invoice, type='date', string='date_invoice'),
        'periode_debut': fields.many2one('account.period','Période début'),
        'periode_fin': fields.many2one('account.period','Période fin'),
        
                }

paiement_anticipe()
