# -*- coding: utf-8 -*-
###################################

from openerp.osv import fields, osv

class account_journal(osv.osv):
    _inherit = 'account.journal'
    
    _columns = {
        'nb_place': fields.integer('Nombre de place dans le parking'),
        'property_payment_term': fields.many2one('account.payment.term', string='Customer Payment Term'),
    }

account_journal()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
