# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from operator import itemgetter
import time
from openerp.osv.fields import many2one
from openerp.tools.translate import _
from openerp import api
import datetime
from dateutil.relativedelta import relativedelta
from openerp import netsvc
import openerp.addons.decimal_precision as dp
from openerp.exceptions import except_orm, Warning, RedirectWarning


class abonnement(osv.osv):
    
    _inherit = "account.analytic.account"

    def on_change_jour (self, cr, uid, ids,jour, context=None):
        journal_pool=self.pool.get('account.journal')
        journal_id=journal_pool.search(cr, uid, [('code','=', 'ABO')])
        nb_place=journal_pool.browse(cr, uid, journal_id, context).nb_place
        if jour:
            if(nb_place <= self.search_count(cr, uid, [('jour', '=', True), ('state', '=', 'open')], context=context)):
                raise except_orm(_('Warning!'), _('Parking complet: Le nombre de place dans le parking pour la période du jour est atteind.'))
        return True
    
    def on_change_nuit (self, cr, uid, ids,nuit, context=None):
        journal_pool=self.pool.get('account.journal')
        journal_id=journal_pool.search(cr, uid, [('code','=', 'ABO')])
        nb_place=journal_pool.browse(cr, uid, journal_id, context).nb_place
        if nuit:
            if(nb_place <= self.search_count(cr, uid, [('nuit', '=', True), ('state', '=', 'open')], context=context)):
                raise except_orm(_('Warning!'), _('Parking complet: Le nombre de place dans le parking pour la période du jour est atteind.'))
        return True
        
    def on_change_partner_id(self, cr, uid, ids, partner_id, name, context=None):
        res = {}
        if partner_id:
            partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            if partner.user_id:
                res['manager_id'] = partner.user_id.id
            if partner.country_id: 
                res['country_id']= partner.country_id.id
            res['street']= partner.street
            res['street2']= partner.street2
            res['phone']= partner.phone
            res['mobile']= partner.mobile
            res['email']= partner.email
            if not name:
                    res['name'] = _('Abonnement: ') + partner.name
        return {'value': res}

    def write(self, cr, uid, ids, vals, context=None):
        journal_pool=self.pool.get('account.journal')
        journal_id=journal_pool.search(cr, uid, [('code','=', 'ABO')])
        nb_place=journal_pool.browse(cr, uid, journal_id, context).nb_place
              
        if vals.get('jour'):
            if(nb_place <= self.search_count(cr, uid, [('jour', '=', True), ('state', '=', 'open')], context=context)):
                raise except_orm(_('Warning!'), _('Parking complet: Le nombre de place dans le parking pour la période du jour est atteind.'))
        if vals.get('nuit'):
            if(nb_place <= self.search_count(cr, uid, [('nuit', '=', True), ('state', '=', 'open')], context=context)):
                raise except_orm(_('Warning!'), _('Parking complet: Le nombre de place dans le parking pour la période du nuit est atteind.'))
        res = super(abonnement, self).write(cr, uid, ids, vals, context=context)
        return res
    

    def _recurring_create_invoice(self, cr, uid, ids, automatic=False, context=None):
        context = context or {}
        invoice_ids = []
        current_date =  time.strftime('%Y-%m-%d')
        obj_period = self.pool.get('account.period')
        ids_period = obj_period.search(cr, uid, [('date_start','<=',current_date),('date_stop','>=',current_date)])
        period=obj_period.browse(cr, uid, ids_period, context=context)
        invoice_line_obj = self.pool.get('account.invoice.line')
        
        if ids:
            contract_ids = ids
        else:
            contract_ids = self.search(cr, uid, [('recurring_next_date','<=', current_date),('state','=', 'open'), ('recurring_invoices','=', True), ('type', '=', 'contract')])
        if contract_ids:
            cr.execute('SELECT company_id, array_agg(id) as ids FROM account_analytic_account WHERE id IN %s GROUP BY company_id', (tuple(contract_ids),))
            for company_id, ids in cr.fetchall():
                for contract in self.browse(cr, uid, ids, context=dict(context, company_id=company_id, force_company=company_id)):
                    try:
                        invoice_line_ids=invoice_line_obj.search(cr, uid, [('account_analytic_id','=',contract.id),('invoice_id.period_id','=',period.id)])
                        if(not invoice_line_ids):
                            invoice_values = self._prepare_invoice(cr, uid, contract, context=context)
                            invoice_ids.append(self.pool['account.invoice'].create(cr, uid, invoice_values, context=context))
                            next_date = datetime.datetime.strptime(contract.recurring_next_date or current_date, "%Y-%m-%d")
                            interval = contract.recurring_interval
                            if contract.recurring_rule_type == 'daily':
                                new_date = next_date+relativedelta(days=+interval)
                            elif contract.recurring_rule_type == 'weekly':
                                new_date = next_date+relativedelta(weeks=+interval)
                            elif contract.recurring_rule_type == 'monthly':
                                new_date = next_date+relativedelta(months=+interval)
                            else:
                                new_date = next_date+relativedelta(years=+interval)
                        self.write(cr, uid, [contract.id], {'recurring_next_date': new_date.strftime('%Y-%m-%d')}, context=context)
                        if automatic:
                            cr.commit()
                    except Exception:
                        if automatic:
                            cr.rollback()
                            _logger.exception('Fail to create recurring invoice for contract %s', contract.code)
                        else:
                            raise
        return invoice_ids
 
    def recurring_create_invoice(self, cr, uid, ids, context=None):      
        invoice_ids = self._recurring_create_invoice(cr, uid, ids, automatic=True, context=context) 
        for invoice_id in invoice_ids:
            netsvc.LocalService('workflow').trg_validate(uid, 'account.invoice', invoice_id, 'invoice_open', cr)
        return invoice_ids
    
    
    def _cron_recurring_create_invoice(self, cr, uid, context=None):
        return self.recurring_create_invoice(cr, uid, [], context=context)

        
    def _recurring_create_invoice_pay_anticipe(self, cr, uid, ids,  period_debut ,automatic=False, context=None):
        context = context or {}
        invoice_ids = []
        current_date =  time.strftime('%Y-%m-%d')
        period_debut_date=period_debut.date_start
        if ids:
            contract_ids = ids
        else:
            contract_ids = self.search(cr, uid, [('recurring_next_date','<=', current_date), ('state','=', 'open'), ('recurring_invoices','=', True), ('type', '=', 'contract')])
        if contract_ids:
            cr.execute('SELECT company_id, array_agg(id) as ids FROM account_analytic_account WHERE id IN %s GROUP BY company_id', (tuple(contract_ids),))
            for company_id, ids in cr.fetchall():
                for contract in self.browse(cr, uid, ids, context=dict(context, company_id=company_id, force_company=company_id)):
                    try:
                        invoice_values = self._prepare_invoice(cr, uid, contract, context=context)
                        invoice_ids.append(self.pool['account.invoice'].create(cr, uid, invoice_values, context=context))
                        next_date = datetime.datetime.strptime(period_debut_date, "%Y-%m-%d")
                        interval = contract.recurring_interval
                        if contract.recurring_rule_type == 'daily':
                            new_date = next_date+relativedelta(days=+interval)
                        elif contract.recurring_rule_type == 'weekly':
                            new_date = next_date+relativedelta(weeks=+interval)
                        elif contract.recurring_rule_type == 'monthly':
                            new_date = next_date+relativedelta(months=+interval)
                        else:
                            new_date = next_date+relativedelta(years=+interval)
                        self.write(cr, uid, [contract.id], {'recurring_next_date': new_date.strftime('%Y-%m-%d')}, context=context)
                        if automatic:
                            cr.commit()
                    except Exception:
                        if automatic:
                            cr.rollback()
                            _logger.exception('Fail to create recurring invoice for contract %s', contract.code)
                        else:
                            raise
        return invoice_ids

#     def recurring_create_invoice_pay_anticipe(self, cr, uid, ids,  period_debut , context=None):
#         return self._recurring_create_invoice_pay_anticipe(cr, uid, ids,  period_debut , context=context)
    
    def recurring_create_invoice_pay_anticipe(self, cr, uid, ids,  period_debut , context=None):      
        invoice_ids = self._recurring_create_invoice_pay_anticipe(cr, uid, ids,  period_debut , context=context) 
        for invoice_id in invoice_ids:
            netsvc.LocalService('workflow').trg_validate(uid, 'account.invoice', invoice_id, 'invoice_open', cr)
        return invoice_ids
        
    def _prepare_invoice_lines(self, cr, uid, contract, invoice_id, context=None):
        invoice_lines = []
        invoice_line_obj = self.pool.get('account.invoice.line')
        if not contract.amount_max:
            raise osv.except_osv(_(u'erreur'), _(u'Le montant est null'))
        amount = contract.amount_max                   
        quantity = 1
        line_value = {                           
                'name':'Abonnement'
              
             }
             
            # Recuperer les valeurs des champs calculer en fonction des produits, clients ...
        line_dict = invoice_line_obj.product_id_change(cr, uid, {},
                            False, False, quantity, '', 'out_invoice', contract.partner_id.id, False, price_unit=amount, context=context)
                
        line_value.update(line_dict['value'])
        line_value['price_unit'] = amount
        line_value['account_analytic_id'] = contract.id
        invoice_lines.append((0, 0, line_value))
        
       #################################
       # line_value['invoice_id'] = invoice_id
        #invoice_line_id = invoice_line_obj.create(cr, uid, line_value, context=context)
        #self.pool.get('account.invoice').write(cr, uid, invoice_id, {'invoice_line': [(6, 0, [invoice_line_id])]}, context=context)
        #netsvc.LocalService('workflow').trg_validate(uid, 'account.invoice', invoice_id, 'invoice_open', cr)
        ##################
        return invoice_lines

    def _prepare_invoice_data(self, cr, uid, contract, context=None):
        context = context or {}

        journal_obj = self.pool.get('account.journal')

        if not contract.partner_id:
            raise osv.except_osv(_('No Customer Defined!'),_("You must first select a Customer for Contract %s!") % contract.name )

        fpos = contract.partner_id.property_account_position or False
        journal_ids = journal_obj.search(cr, uid, [('code', '=','ABO')], limit=1)

        if not journal_ids:
            raise osv.except_osv(_('Error!'),
            _('Please define a sale journal for the company "%s".') % (contract.company_id.name or '', ))

        partner_payment_term = contract.partner_id.property_payment_term and contract.partner_id.property_payment_term.id or False

        currency_id = False
        if contract.pricelist_id:
            currency_id = contract.pricelist_id.currency_id.id
        elif contract.partner_id.property_product_pricelist:
            currency_id = contract.partner_id.property_product_pricelist.currency_id.id
        elif contract.company_id:
            currency_id = contract.company_id.currency_id.id

        invoice = {
           'account_id': contract.partner_id.property_account_receivable.id,
           'type': 'out_invoice',
           'partner_id': contract.partner_id.id,
           #'vehicule_id':contract.vehicule_id.id or False,
           'currency_id': currency_id,
           'journal_id': len(journal_ids) and journal_ids[0] or False,
           'date_invoice': contract.recurring_next_date,
           'origin': contract.code,
           'fiscal_position': fpos and fpos.id,
           'payment_term': partner_payment_term,
           'state': 'open',
           'credit': contract.amount_max,
           'company_id': contract.company_id.id or False,
        }
        return invoice        
    
    def _prepare_invoice(self, cr, uid, contract, context=None):
        invoice = self._prepare_invoice_data(cr, uid, contract, context=context)
        invoice['invoice_line'] = self._prepare_invoice_lines(cr, uid, contract, invoice['fiscal_position'], context=context)
        
        return invoice
        
    
    _columns = {
        'country_id': fields.many2one('res.country', 'Country'),
        'street': fields.char('Street'),
        'street2': fields.char('Street2'),
        'phone': fields.char('Phone'),
        'mobile': fields.char('Mobile'),
        'email': fields.char('Email'),
        'jour': fields.boolean("Jour"),# True l'abonnement pour le jour seulemet
        'nuit': fields.boolean("Nuit"),# True l'abonnement pour la nuit seulemet
        'vehicule_id': fields.many2one('vehicule', 'Véhicule', help='Véhicule'),
        }
    
    
    _defaults = {
                'fix_price_invoices': True,
                'recurring_invoices': True,
        }
    